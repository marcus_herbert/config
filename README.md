#Config Files
These are some of my config files. Feel free to use them, or modify them, at will.

##OS:
* Fedora 28

##Prerequisite Packages:
* rxvt-unicode
* xmodmap

##Setup urxvt for transparency and an easy read font:
####URxvt
1. sudo dnf -y install rxvt-unicode
2. ln -s repo/path/.Xdefaults ~/.Xdefaults

##Swap caps lock and escape key
1. cp repo/path/.Xmodmap ~/.Xmodmap
2. xmodmap ~/.Xmodmap
